import * as React from 'react';

interface IProps {
    title: string;
}

export default class SimpleComponents extends React.Component<IProps, any>{
    render(){
        return (
            <div>
                <label>Title: </label>
                <span>{this.props.title}</span>
            </div>
        );
    }
}