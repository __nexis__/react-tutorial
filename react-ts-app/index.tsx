import * as React from 'react';
import * as ReactDOM from 'react-dom';

import SimpleComponent from './components/SimpleComponent';

class TestApp extends React.Component<any, any>{
    render(){
        return (
            <div>
                Simple App
                <SimpleComponent title={'The Matrix(1999)'} />
            </div>
        );
    }
}

ReactDOM.render(
    <TestApp/>,
    document.getElementById("root")
);

