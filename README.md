You need to run the following commands for project setup:

-- install deps

```
#!bash

npm install -g webpack
npm install -g typescript
npm install

```

To start the project run:


```
#!bash

npm run devserver
```
Check out the link for more info about devserver - [Webpack Devserver](https://bitbucket.org/__nexis__/react-tutorial/wiki/Webpack%20&%20Webpack%20DevServer)


and open react-ts-app/index.html