var path = require('path');
var webpack = require('webpack');

module.exports = {

    entry: {
        main: [ path.resolve(__dirname, 'react-ts-app/index.tsx') ],
        vendor: [
            'react',
            'react-dom'
        ]
    },

    output: {
        path: path.resolve(__dirname, 'dist/'),
        filename: '[name].js',
        chunkFilename: '[id].chunk.js',
        publicPath: '/dist/'
    },

    // Enable sourcemaps for debugging webpack's output.
    devtool: "eval-cheap-module-source-map",

    plugins: [
		new webpack.ProvidePlugin({
			$: "jquery",
			jQuery: "jquery"
    	})
	],

    resolve: {

        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: ["", ".webpack.js", ".web.js", ".ts", ".tsx", ".js"]
    },

    module: {
        loaders: [
            // All files with a '.ts' or '.tsx' extension will be handled by 'ts-loader'.
            {
                include: [ path.resolve(__dirname, 'react-ts-app')],
                test: /\.tsx?$/, loader: "ts-loader"
            }
        ]
    }
};