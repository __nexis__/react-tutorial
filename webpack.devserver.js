var webpack          = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var config           = require('./webpack.config');

var port             = 3010;

config.entry.main.unshift("webpack-dev-server/client?http://localhost:" + port);
config.output.publicPath = "http://localhost:" + port + config.output.publicPath;

new WebpackDevServer(webpack(config), {
  publicPath: config.output.publicPath,
  hot: true,
  historyApiFallback: true,
  stats: {
    colors: true
  }
}).listen(port, 'localhost', function (err) {
  if (err) {
    console.log(err);
  }

  console.log('Listening at localhost:' + port);
});